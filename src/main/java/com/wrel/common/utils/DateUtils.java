/**
 * 
 */
package com.wrel.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * @author 10503305
 *
 */
public class DateUtils {

    public enum DateFormat {

        MONTH_DAY_YEAR("MM/dd/yyyy"),;

        private String pattern;

        DateFormat(final String pattern) {
            this.pattern = pattern;
        }

        public String getPattern() {
            return this.pattern;
        }

    }

    public static Date strToDate(final String str, final DateFormat dateFormat) {
        if (StringUtils.isBlank(str)) {
            return null;

        } else {
            try {
                return getSimpleDateFormat(dateFormat).parse(str);
            } catch (ParseException e) {
                // TODO 自動產生 catch 區塊
                e.printStackTrace();
                return null;
            }
        }
    }

    private static SimpleDateFormat getSimpleDateFormat(final DateFormat dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat.getPattern());
        return sdf;
    }

}
