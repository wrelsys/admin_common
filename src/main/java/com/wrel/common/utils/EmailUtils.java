/**
 * 
 */
package com.wrel.common.utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 10503305
 *
 */
@Component
public class EmailUtils {

    private final Logger LOGGER = LoggerFactory.getLogger(EmailUtils.class);

    private Properties props;

    @Value("${mail.smtp.auth}")
    private String auth;

    @Value("${mail.smtp.starttls.enable}")
    private String starttls;

    @Value("${mail.smtp.host}")
    private String host;

    @Value("${mail.smtp.port}")
    private String port;

    @Value("${mail.smtp.account}")
    private String account;

    @Value("${mail.smtp.password}")
    private String password;

    @Value("${mail.from}")
    private String from;

    public boolean sendMail(final String receiver, final String subject, final String content) {
        final Session mailSession = this.getMailSession();
        final Message message = new MimeMessage(mailSession);
        boolean flag = true;
        try {
            message.setFrom(new InternetAddress(this.from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver));
            message.setSubject(subject);
            message.setText(content);

            final Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, Integer.valueOf(port), account, password);
            Transport.send(message);

        } catch (AddressException e) {
            LOGGER.error(e.getMessage());
            flag = false;
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage());
            flag = false;
        }

        return flag;
    }

    private Session getMailSession() {

        if (this.props == null) {
            props = new Properties();
            props.put("mail.smtp.auth", auth);
            LOGGER.debug(auth);
            LOGGER.debug(host);
            props.put("mail.smtp.starttls.enable", starttls);
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
        }

        return Session.getInstance(this.props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(account, password);
            }
        });
    }

}
