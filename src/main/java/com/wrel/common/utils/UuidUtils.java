
package com.wrel.common.utils;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Page/Class Name: UuidUtils
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月16日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月16日
 * Version 1.0
 *
 */
public class UuidUtils {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    public static final  String getUUID(){
        return  StringUtils.replace(UUID.randomUUID().toString(), "-", "");
    }
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}

