
package com.wrel.admin.service;

import com.wrel.admin.entity.ActionHistory;

/**
 *
 * Page/Class Name: ActionHistoryService
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
public interface ActionHistoryService {
    //================================================
    //== [Method] Block Start
    //====
    void insertActionHistory(ActionHistory actionHistory);
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
