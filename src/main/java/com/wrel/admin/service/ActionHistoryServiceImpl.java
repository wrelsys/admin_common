
package com.wrel.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wrel.admin.dao.ActionHistoryDao;
import com.wrel.admin.entity.ActionHistory;

/**
 *
 * Page/Class Name: ActionHistoryServiceImpl
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Service
@Transactional
public class ActionHistoryServiceImpl implements ActionHistoryService {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private ActionHistoryDao actionHistoryDao;
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====

    @Override
    public final void insertActionHistory(final ActionHistory actionHistory) {
        this.actionHistoryDao.save(actionHistory);

    }

    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
