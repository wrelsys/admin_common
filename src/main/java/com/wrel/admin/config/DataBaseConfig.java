
package com.wrel.admin.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DataBaseConfig {
    // ================================================
    // == [Enumeration types] Block Start
    // ====
    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====

    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    @Value("${db.driver}")
    private String dbDriver;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.username}")
    private String dbUsername;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${hibernate.dialect}")
    private String hibernateDialect;

    @Value("${hibernate.show_sql}")
    private String hibernateShowSql;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHbm2ddlAuto;

    @Value("${entity.packages}")
    private String entityPackages;

    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (嚙踐�nit method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(this.dbDriver);
        dataSource.setUrl(this.dbUrl);

        dataSource.setUsername(this.dbUsername);
        dataSource.setPassword(this.dbPassword);

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { this.entityPackages });
        sessionFactory.setHibernateProperties(additionalProperties());
        return sessionFactory;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", this.hibernateDialect);
        properties.put("hibernate.show_sql", this.hibernateShowSql);
        properties.put("hibernate.hbm2ddl.auto", this.hibernateHbm2ddlAuto);

        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }
    // ####################################################################
    // ## [Method] sub-block :

    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
