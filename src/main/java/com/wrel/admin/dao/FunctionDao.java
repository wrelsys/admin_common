
package com.wrel.admin.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.wrel.admin.entity.Function;

/**
 * The Class FunctionDao.
 */
@Repository
public class FunctionDao extends BaseDao<Function> {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====

    /**
     * Gets the function by id.
     *
     * @param functionId the function id
     * @return the function by id
     */
    public Function getFunctionById(Long functionId) {
        final Function function = this.get(Function.class, functionId);
        Hibernate.initialize(function.getParent());
        Hibernate.initialize(function.getSubFunctions());
        if (function.getParent() != null) {
            function.setParentId(function.getParent().getId());
        }
        return function;
    }

    /**
     * Gets the all functions.
     *
     * @param init the init
     * @return the all functions
     */
    public    List<Function> getAllFunctions(boolean init) {
        final StringBuilder str = new StringBuilder();
        str.append("FROM Function WHERE parent is null ");
        str.append("ORDER BY order ");
        Session session = this.getSession();
        final Query query = session.createQuery(str.toString());

        @SuppressWarnings("unchecked")
        final List<Function> functions = query.list();

        for (Function function : functions) {

            Hibernate.initialize(function.getSubFunctions());
        }

        return functions;
    }

    /**
     * Gets the function is parent.
     *
     * @param parentId the parent id
     * @return the function is parent
     */
    public Function getFunctionIsParent(final Long parentId) {
        final StringBuilder str = new StringBuilder();
        str.append("FROM Function WHERE parent is null ");
        str.append("  AND id = :parentId ");

        final Query query = this.getSession().createQuery(str.toString());
        query.setLong("parentId", parentId);
        return (Function) query.uniqueResult();

    }
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
