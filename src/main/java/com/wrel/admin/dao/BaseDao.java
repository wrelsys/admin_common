/**
 * 
 */
package com.wrel.admin.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class BaseDao.
 *
 * @author 10503305
 * @param <T> the generic type
 */
public class BaseDao<T> {

    /** The session factory. */
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gets the session.
     *
     * @return the session
     */
    protected Session getSession() {

        return this.sessionFactory.getCurrentSession();
    }

    /**
     * Save.
     *
     * @param clz the clz
     */
    public final T save(T clz) {
        this.getSession().save(clz);
        return clz;
    }

    /**
     * Update.
     *
     * @param clz the clz
     */
    public final T update(T clz) {
        this.getSession().update(clz);
        return clz;
    }

    /**
     * Delete.
     *
     * @param clz the clz
     */
    public final T delete(T clz) {
        this.getSession().delete(clz);
        return clz;
    }

    /**
     * Save or update.
     *
     * @param clz the clz
     */
    public final T saveOrUpdate(T clz) {
        this.getSession().save(clz);
        return clz;
    }

    /**
     * Gets the.
     *
     * @param clz the clz
     * @param id the id
     * @return the t
     */
    public final T get(Class<T> clz, long id) {
        return this.getSession().get(clz, id);
    }
}
