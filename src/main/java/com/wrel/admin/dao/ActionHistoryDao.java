
package com.wrel.admin.dao;

import org.springframework.stereotype.Repository;

import com.wrel.admin.entity.ActionHistory;

/**
 *
 * Page/Class Name: ActionHistoryDao
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Repository
public class ActionHistoryDao extends BaseDao<ActionHistory> {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
