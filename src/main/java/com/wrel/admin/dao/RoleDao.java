
package com.wrel.admin.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.wrel.admin.entity.Role;

/**
 *
 * Page/Class Name: RoleDao
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
@Repository
public class RoleDao extends BaseDao<Role> {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    public final List<Role> getAllRoles() {
        final Query query = this.getSession().createQuery("FROM Role");
        @SuppressWarnings("unchecked")
        final List<Role> results = query.list();
        return results;
    }

    public final Role getRoleByRoleId(final String roleId) {
        final Query query = this.getSession().createQuery("FROM Role WHERE LOWER(roleId) = :roleId ");
        query.setString("roleId", roleId);
        return (Role) query.uniqueResult();
    }
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
