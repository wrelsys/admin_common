
package com.wrel.admin.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.wrel.admin.entity.User;
import com.wrel.admin.entity.User.Status;

/**
 *
 * Page/Class Name: UserJpaDao Title: Description: author: weiting Create Date:
 * 2016撟�4���3� Last Modifier: eldar Last Modify Date: 2016撟�4���3� Version
 * 1.0
 *
 */
@Repository
public class UserDao extends BaseDao<User> {

    public final User getUserByEmailAndStatus(final String email, final Status status) {

        final StringBuilder str = new StringBuilder();
        str.append("FROM User WHERE LOWER(email) = :email");
        if (status != null) {
            str.append(" AND status = :status");
        }

        final Query query = this.getSession().createQuery(str.toString());
        query.setParameter("email", email);
        if (status != null) {
            query.setParameter("status", status.getValue());
        }

        final User user = (User) query.uniqueResult();
        return user;

    }

    public final List<User> queryUser(final User queryUser) {

        final StringBuilder str = new StringBuilder();
        str.append(" FROM User WHERE 1=1 ");
        if (StringUtils.isNotBlank(queryUser.getEmail())) {

            str.append(" AND email like :email");
        }
        if (queryUser.getStatus() != null) {
            str.append(" AND status = :status");
        }
        if (queryUser.getStartCreateDate() != null) {
            str.append(" AND createTime >= :startCreateDate");
        }
        if (queryUser.getEndCreateDate() != null) {
            str.append(" AND createTime <= :endCreateDate");
        }
        final Query query = this.getSession().createQuery(str.toString());
        if (StringUtils.isNotBlank(queryUser.getEmail())) {

            query.setString("email", queryUser.getEmail());
        }
        if (queryUser.getStatus() != null) {
            query.setInteger("status", queryUser.getStatus());
        }
        if (queryUser.getStartCreateDate() != null) {
            query.setDate("startCreateDate", queryUser.getStartCreateDate());
        }
        if (queryUser.getEndCreateDate() != null) {
            query.setDate("endCreateDate", queryUser.getEndCreateDate());
        }

        @SuppressWarnings("unchecked")
        final List<User> users = query.list();

        return users;

    }
    // ================================================
    // == [Enumeration types] Block Start
    // ====
    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (�init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
