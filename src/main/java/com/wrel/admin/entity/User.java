package com.wrel.admin.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * Page/Class Name: User Title: Description: author: weiting Create Date:
 * 2016年3月26日 Last Modifier: eldar Last Modify Date: 2016年3月26日 Version 1.0
 *
 */
@Entity
@Table(name = "wrel_users")
public class User extends BaseEntity implements Serializable {

    // ================================================
    // == [Enumeration types] Block Start
    // ====
    public enum Status {
        NON_ACTIVE(0, "停用"), //
        ACTIVE(1, "啟用"), //
        NON_VERIFY(2, "驗證中");

        private int value;

        private String title;

        public int getValue() {
            return this.value;
        }

        public String getTitle() {
            return this.title;
        }

        Status(final int value, final String title) {
            this.value = value;
            this.title = title;
        }

        public static Status lookup(final int value) {
            final Status statuses[] = Status.values();
            Status returnStatus = null;
            for (Status status : statuses) {
                if (status.value == value) {
                    returnStatus = status;
                    break;
                }

            }
            return returnStatus;
        }
    }

    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = 1L;

    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Column(name = "name", nullable = false, length = 10)
    private String name;

    @Column(name = "email", nullable = false, length = 30)
    private String email;

    @Column(name = "status", nullable = false, precision = 1, scale = 0)
    private Integer status;

    @Column(name = "tel", nullable = false, length = 10)
    private String tel;

    @Column(name = "mobile", length = 15)
    private String mobile;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_time", nullable = false)
    private Date modifyTime;

    @Transient
    private String showStatus;

    @Transient
    private Date startCreateDate;

    @Transient
    private Date endCreateDate;

    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====

    public User() {

    }

    public User(String name, String password, String email, String tel, String mobile, Status status) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.mobile = mobile;
        this.tel = tel;
        this.status = status.getValue();
    }

    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (含init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return this.modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getStartCreateDate() {
        return this.startCreateDate;
    }

    public void setStartCreateDate(Date startCreateDate) {
        this.startCreateDate = startCreateDate;
    }

    public Date getEndCreateDate() {
        return this.endCreateDate;
    }

    public void setEndCreateDate(Date endCreateDate) {
        this.endCreateDate = endCreateDate;
    }

    public String getShowStatus() {

        return Status.lookup(this.status).getTitle();
    }

    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
