
package com.wrel.admin.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * Page/Class Name: Functions
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Entity
@Table(name = "wrel_functions")
public class Function extends BaseEntity implements Serializable {

    //================================================
    //== [Enumeration types] Block Start
    //====
    public enum Show {
        YES(true), NO(false);
        private boolean value;

        Show(boolean value) {
            this.value = value;
        }

        public boolean isValue() {
            return this.value;
        }

        public static Show lookup(final String title) {
            final Show[] shows = Show.values();
            Show returnShow = null;
            for (Show show : shows) {
                if (StringUtils.equals(show.toString(), title)) {
                    returnShow = show;
                    break;
                }

            }
            return returnShow;
        }

        public static Show lookup(final boolean value) {
            final Show[] shows = Show.values();
            Show returnShow = null;
            for (Show show : shows) {
                if (show.isValue() == value) {
                    returnShow = show;
                    break;
                }

            }
            return returnShow;
        }
    }

    public enum Status {
        NON_ACTIVE(0, "停用"), ACTIVE(1, "啟用");

        private int value;

        private String title;

        public int getValue() {
            return this.value;
        }

        public String getTitle() {
            return this.title;
        }

        Status(final int value, final String title) {
            this.value = value;
            this.title = title;
        }

        public static Status lookup(final Integer value) {
            if (value == null) {
                return null;
            }
            final Status[] statuses = Status.values();
            Status returnStatus = null;
            for (Status status : statuses) {
                if (value == status.getValue()) {
                    returnStatus = status;
                    break;
                }
            }
            return returnStatus;
        }
    }

    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = -1206906240141713390L;

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "function_name", nullable = false, length = 20)
    private String functionName;

    @Column(name = "function_url", length = 50)
    private String functionUrl;

    @Column(name = "status", nullable = false, precision = 1, scale = 0)
    private int status;

    @ManyToOne(cascade = { CascadeType.DETACH })
    @JoinColumn(name = "parent_id")
    @JsonBackReference
    private Function parent;

    @Column(name = "`order`", precision = 1, scale = 0)
    private int order;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date", nullable = false)
    private Date modifyDate;

    @Column(name = "`show`", nullable = false)
    private Boolean show;

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
    @OrderBy("`order`")
    @JsonManagedReference
    private Set<Function> subFunctions;

    @Transient
    private String showStatus;

    @Transient
    private String showStr;

    @Transient
    private Long parentId;
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====

    public String getShowStr() {

        final Show show = Show.lookup(this.show);
        if (show != null) {
            return show.toString();
        }
        return StringUtils.EMPTY;

    }

    public Function() {
    }

    public Function(final String functionName, final String functionUrl, final boolean show, final Integer status,
            final Integer order, final Set<Function> subFunctions) {
        this.functionName = functionName;
        this.functionUrl = functionUrl;
        this.show = show;
        this.status = status;
        if (order != null) {
            this.order = order;
        }
        if (subFunctions != null) {
            this.subFunctions = subFunctions;
        }
    }

    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getFunctionName() {
        return this.functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionUrl() {
        return this.functionUrl;
    }

    public void setFunctionUrl(String functionUrl) {
        this.functionUrl = functionUrl;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return this.modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Function getParent() {
        return this.parent;
    }

    public void setParent(Function parent) {
        this.parent = parent;
    }

    public Set<Function> getSubFunctions() {
        return this.subFunctions;
    }

    public void setSubFunctions(Set<Function> subFunctions) {
        this.subFunctions = subFunctions;
    }

    public String getShowStatus() {
        final Status status = Status.lookup(this.status);
        if (status != null) {
            return status.getTitle();
        }
        return StringUtils.EMPTY;
    }

    public void setShowStatus(String showStatus) {
        this.showStatus = showStatus;
    }
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
