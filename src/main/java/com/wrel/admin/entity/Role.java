
package com.wrel.admin.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "wrel_roles")
public class Role extends BaseEntity implements Serializable {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	public enum Status {
		NON_ACTIVE(0, "停用"), ACTIVE(1, "啟用");

		private int value;

		private String title;

		public int getValue() {
			return this.value;
		}

		public String getTitle() {
			return this.title;
		}

		Status(final int value, final String title) {
			this.value = value;
			this.title = title;
		}

		public static Status lookup(final Integer value) {
			if (value == null) {
				return null;
			}
			final Status[] statuses = Status.values();
			Status returnStatus = null;
			for (Status status : statuses) {
				if (value == status.getValue()) {
					returnStatus = status;
					break;
				}
			}
			return returnStatus;
		}
	}

	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1L;

	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "role_id", nullable = false, length = 15)
	private String roleId;

	@Column(name = "role_name", nullable = false, length = 15)
	private String roleName;

	@Column(name = "status", nullable = false, precision = 1, scale = 0)
	private int status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = false)
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modify_date", nullable = false)
	private Date modifyDate;

	@Transient
	private String showStatus;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	public Role() {

	}

	public Role(String roleId, String roleName, int status) {

		this.roleId = roleId;
		this.roleName = roleName;
		this.status = status;
	}

	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRoleId() {
		return this.roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getShowStatus() {
		final Status status = Status.lookup(this.status);
		if (status != null) {
			return status.getTitle();
		}
		return StringUtils.EMPTY;
	}

	public void setShowStatus(String showStatus) {
		this.showStatus = showStatus;
	}
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
