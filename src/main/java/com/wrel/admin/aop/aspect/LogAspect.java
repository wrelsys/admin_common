
package com.wrel.admin.aop.aspect;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wrel.admin.common.LoginInfo;
import com.wrel.admin.entity.ActionHistory;
import com.wrel.admin.entity.ActionHistory.ProjectType;
import com.wrel.admin.entity.User;
import com.wrel.admin.service.ActionHistoryService;

/**
 *
 * Page/Class Name: LogHandler
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Component
@Aspect
public class LogAspect {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private ActionHistoryService actionHistoryService;
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====

    //    @Before("within(*com.wrel.admin.*.controller.* ) && " + //
    //            "execution(@org.springframework.web.bind.annotation.RequestMapping * *(..)) && " + //
    //            "@annotation(requestMapping) && " + //
    //            "execution(@com.wrel.admin.annotation.Logable * *(..)) && " + //
    //            "@annotation(logable)")
    @Before("within(*com.wrel.admin.api..* ) && " + //
            "execution(@org.springframework.web.bind.annotation.RequestMapping * *(..)) && " + //
            "@annotation(requestMapping) && " + //
            " args(..)")
    public void logApiController(JoinPoint joinPoint, RequestMapping requestMapping) {
        final Object[] params = joinPoint.getArgs();
        final String[] paths = requestMapping.value();
        final RequestMethod[] methods = requestMapping.method();
        this.actionHistoryService.insertActionHistory(new ActionHistory(//
                this.getUrl(paths), //
                this.getMethod(methods), //
                this.getParamsStr(params), //
                null, //
                ProjectType.API//
        ));

    }

    @Before("within(*com.wrel.admin.console..* ) && " + //
            "execution(@org.springframework.web.bind.annotation.RequestMapping * *(..)) && " + //
            "@annotation(requestMapping) && " + //
            " args(..)")
    public void logConsoleController(JoinPoint joinPoint, RequestMapping requestMapping) {

        final Object[] params = joinPoint.getArgs();
        final String[] paths = requestMapping.value();
        final StringBuilder str = new StringBuilder();
        for (String path : paths) {
            str.append(path).append(";");
        }
        final User user = this.getLoginInfo();
        final RequestMethod[] methods = requestMapping.method();
        this.actionHistoryService.insertActionHistory(new ActionHistory(//
                this.getUrl(paths), //
                this.getMethod(methods), //
                this.getParamsStr(params), user, //
                ProjectType.CONSOLE));
    }
    //####################################################################
    //## [Method] sub-block :

    private String getParamsStr(final Object[] params) {
        final StringBuilder str = new StringBuilder();
        for (Object param : params) {
            if (param != null) {
                str.append(param.toString()).append(";");
            }
        }
        return str.toString();
    }

    private String getUrl(final String[] paths) {
        final StringBuilder str = new StringBuilder();
        for (String path : paths) {
            str.append(path).append(";");
        }
        return str.toString();
    }

    private String getMethod(final RequestMethod[] methods) {
        final StringBuilder str = new StringBuilder();
        for (RequestMethod method : methods) {
            str.append(method.toString()).append(";");
        }
        return str.toString();
    }

    private User getLoginInfo() {
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (StringUtils.equals("anonymousUser", String.valueOf(obj))) {
            return null;
        } else {
            final LoginInfo loginInfo = (LoginInfo) obj;
            return loginInfo.getUser();
        }
    }
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
