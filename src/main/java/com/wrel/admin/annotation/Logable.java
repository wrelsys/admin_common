
package com.wrel.admin.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.commons.lang3.StringUtils;

/**
*
* Page/Class Name: Logable
* Title:
* Description:
* Copyright: 
* Company:	
* author: 10503305 
* Create Date:	2016年4月9日
* Last Modify Date: 2016年4月9日
* Version 1.0
*
*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface Logable
    {

    String value() default StringUtils.EMPTY;

   // ProjectType type();

    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
    }
